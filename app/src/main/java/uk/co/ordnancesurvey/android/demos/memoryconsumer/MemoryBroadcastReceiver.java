package uk.co.ordnancesurvey.android.demos.memoryconsumer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by rex on 11/09/2017.
 */

public class MemoryBroadcastReceiver extends BroadcastReceiver {
    private static MemoryInterface callback;


    public static void setMemoryListener(MemoryInterface listener) {
        MemoryBroadcastReceiver.callback = listener;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        long[] memory = intent.getLongArrayExtra("memory");


        //  MEMORY 2/MEMORY1 = AVAILABLE MEMORY IN FLOAT 0-1;
        if ((float) memory[2] / (float) memory[1] < 0.95) {

            if (callback != null) {
                callback.onMemoryLow();
            }
            System.out.println("memory is low");

        }
    }

    public interface MemoryInterface {
        void onMemoryLow();
    }
}

