package uk.co.ordnancesurvey.android.demos.memoryconsumer;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private boolean isRunning;
    private Button button;
    private TextView value;
    private List<LinearLayout> longs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        value = (TextView) findViewById(R.id.memory);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRunning) {
                    stopMemoryConsuming();
                } else {
                    startMemoryConsuming();
                }
            }
        });
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (isRunning) {
                        final long[] memory = availableMemory();
                        if (memory[0] < 480) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
//                            Intent intent = new Intent("uk.co.yotta.MEMORY_CONSUMER");
//                            intent.putExtra("memory", memory);
//                            sendBroadcast(intent);
                                    value.setText("Memory consumed: " + memory[0] + " mb");
                                }
                            });


                            for (int i = 0; i < 100; i++) {
                                longs.add(new LinearLayout(MainActivity.this));
                            }
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
        thread.start();

    }

    // position 0 : usedMB
    // position 1: maxHeap
    // position 2: availableHeap
    private static long[] availableMemory() {
        final Runtime runtime = Runtime.getRuntime();
        final long usedMemInMB = (runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
        final long maxHeapSizeInMB = runtime.maxMemory() / 1048576L;
        final long availHeapSizeInMB = maxHeapSizeInMB - usedMemInMB;
        long[] memory = new long[3];

        memory[0] = usedMemInMB;
        memory[1] = maxHeapSizeInMB;
        memory[2] = availHeapSizeInMB;
        return memory;

    }

    private void startMemoryConsuming() {
        isRunning = true;
        button.setText("Stop Consuming!");
    }

    private void stopMemoryConsuming() {
        isRunning = false;
        button.setText("Start Consuming!");
    }

}
